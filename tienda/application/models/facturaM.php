<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class facturaM extends CI_Model {
public function get_factura(){
	$this->db->select('num_factura,nombre,apellido,fecha,nombres');
	$this->db->from('factura f');
	$this->db->join('cliente c','c.id_cliente=f.id_cliente');
	$this->db->join('modo_pago m','m.num_pago=f.num_pago');
	$exe = $this->db->get();
	return $exe->result();
}

public function eliminar($id){
	$this->db->where('num_factura',$id);
	$this->db->delete('factura');
	if($this->db->affected_rows()>0){
		return "eli";
	}else{
		return false;
	}
}

public function get_pago(){
	$exe = $this->db->get('modo_pago');
	return $exe->result();
}

public function get_cliente(){
	$exe = $this->db->get('cliente');
	return $exe->result();
}

public function ingresar($datos){
	$sp_insercion_factura ="CALL sp_insercion_factura(?,?,?)";
	$arreglo = array('id_cliente'=>$datos['id_cliente'],
		'fecha'=>$datos['fecha'],
         'num_pago'=>$datos['num_pago']);
$query = $this->db->query($sp_insercion_factura,$arreglo);
	if($query!==null){
		return "add";
	}else{
		return false;
	}
}

public function get_datos($id){
	$this->db->where('num_factura',$id);
	$exe = $this->db->get('factura');
	return $exe->result();
}

public function actualizar($datos){
	$this->db->set('id_cliente',$datos['id_cliente']);
	$this->db->set('fecha',$datos['fecha']);
	$this->db->set('num_pago',$datos['num_pago']);
	$this->db->where('num_factura',$datos['num_factura']);
	$this->db->update('factura');

	if($this->db->affected_rows()>0){
		return "edi";
	}else{
		return false;
	}
}
}
