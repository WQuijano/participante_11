<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class productoM extends CI_Model {
public function get_producto(){
	$this->db->select('p.id_producto,p.nombre_producto,p.precio,p.stock,nombres');
	$this->db->from('producto p');
	$this->db->join('categoria c','c.id_categoria=p.id_categoria');
	$exe = $this->db->get();
	return $exe->result();
}

public function get_categoria(){
	$exe = $this->db->get('categoria');
	return $exe->result();
}

public function eliminar($id){
	$this->db->where('id_producto',$id);
	$this->db->delete('producto');
	if($this->db->affected_rows()>0){
		return "eli";
	}else{
		return false;
	}
}


public function ingresar($datos){
	$this->db->set('nombre_producto',$datos['nombre_producto']);
	$this->db->set('precio',$datos['precio']);
	$this->db->set('stock',$datos['stock']);
	$this->db->set('id_categoria',$datos['categoria']);
	$this->db->insert('producto');
	if($this->db->affected_rows()>0){
		return "add";
	}else{
		return false;
	}
}

public function get_datos($id){
	$this->db->where('id_producto',$id);
	$exe = $this->db->get('producto');
	return $exe->result();
}

public function actualizar($datos){
	$this->db->set('nombre_producto',$datos['nombre_producto']);
	$this->db->set('precio',$datos['precio']);
	$this->db->set('stock',$datos['stock']);
	$this->db->set('id_categoria',$datos['categoria']);
	$this->db->where('id_producto',$datos['id_producto']);
	$this->db->update('producto');
	if($this->db->affected_rows()>0){
		return "edi";
	}else{
		return false;
	}
}

}
