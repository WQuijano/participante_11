<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class clienteM extends CI_Model {
public function get_cliente(){
	$this->db->select('c.id_cliente,c.nombre,c.apellido,c.direccion,c.fecha_nacimiento,c.telefono,c.email');
	$this->db->from('cliente c');
	$exe = $this->db->get();
	return $exe->result();
}

public function get_factura($id){
	$this->db->select('f.num_factura,c.nombre,c.apellido,f.fecha,m.nombres');
	$this->db->from('factura f');
	$this->db->join('cliente c','c.id_cliente=f.id_cliente');
	$this->db->join('modo_pago m','m.num_pago=f.num_pago');
	$this->db->where('f.id_cliente',$id);
	$exe = $this->db->get();
	return $exe->result();
}

public function eliminar($id){
	$this->db->where('id_cliente',$id);
	$this->db->delete('cliente');
	if($this->db->affected_rows()>0){
		return "eli";
	}else{
		return false;
	}
}


public function verFactura($id){
	$this->db->select('d.num_detalle,p.nombre_producto,d.cantidad,d.precio,f.num_factura,c.nombre,c.apellido,f.fecha,m.nombres');
	$this->db->from('detalle d');
	$this->db->join('producto p','p.id_producto=d.id_producto');
	$this->db->join('factura f','f.num_factura=d.id_factura');
	$this->db->join('cliente c','c.id_cliente=f.id_cliente');
	$this->db->join('modo_pago m','m.num_pago=f.num_pago');
	$this->db->where('d.id_factura',$id);
	$exe = $this->db->get();
	return $exe->result();
}

public function get_pago(){
	$exe = $this->db->get('modo_pago');
	return $exe->result();
}

public function get_clientes(){
	$exe = $this->db->get('cliente');
	return $exe->result();
}


public function eliminarF($id){
	$this->db->where('num_detalle',$id);
	$this->db->delete('detalle');
	if($this->db->affected_rows()>0){
		return "elim";
	}else{
		return false;
	}
}

public function ingresar($datos){
	$this->db->set('nombre',$datos['nombre']);
	$this->db->set('apellido',$datos['apellido']);
	$this->db->set('direccion',$datos['direccion']);
	$this->db->set('telefono',$datos['telefono']);
	$this->db->set('fecha_nacimiento',$datos['fecha']);
	$this->db->set('email',$datos['correo']);
	$this->db->insert('cliente');
	if($this->db->affected_rows()>0){
		return "add";
	}else{
		return false;
	}
}

public function compra($datos){
	$this->db->set('id_factura',$datos['id_factura']);
	$this->db->set('id_producto',$datos['id_producto']);
	$this->db->set('cantidad',$datos['cantidad']);
	$this->db->set('precio',$datos['precio']);
	$this->db->insert('detalle');
	if($this->db->affected_rows()>0){
		return "compra";
	}else{
		return false;
	}
}

public function get_datos($id){
	$this->db->where('id_cliente',$id);
	$exe = $this->db->get('cliente');
    return $exe->result();
}

public function set_compra($id){
	$this->db->where('id_cliente',$id);
	$exe = $this->db->get('cliente');
    return $exe->result();
}

public function get_producto(){
	$exe = $this->db->get('producto');
    return $exe->result();
}

public function actualizar($datos){
	$this->db->set('nombre',$datos['nombre']);
	$this->db->set('apellido',$datos['apellido']);
	$this->db->set('direccion',$datos['direccion']);
	$this->db->set('telefono',$datos['telefono']);
	$this->db->set('fecha_nacimiento',$datos['fecha']);
	$this->db->set('email',$datos['correo']);
	$this->db->where('id_cliente',$datos['id_cliente']);
	$this->db->update('cliente');
	if($this->db->affected_rows()>0){
		return "edi";
	}else{
		return false;
	}
}

}
