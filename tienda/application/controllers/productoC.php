<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class productoC extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('productoM');
	}
	public function index()
	{
		$data = array('title'=>'Tienda || producto',
			'producto'=>$this->productoM->get_producto(),
			'categoria'=>$this->productoM->get_categoria());
		$this->load->view('template/header',$data);
		$this->load->view('productoV');
		$this->load->view('template/footer');
	}
public function eliminar($id){
		$respuesta = $this->productoM->eliminar($id);
		$data = array('title'=>'Tienda || producto',
			'producto'=>$this->productoM->get_producto(),
			'categoria'=>$this->productoM->get_categoria(),
		     'msj'=>$respuesta);
		$this->load->view('template/header',$data);
		$this->load->view('productoV');
		$this->load->view('template/footer');

	}

	public function ingresar(){
	$datos['nombre_producto'] = $this->input->post('nombre');
	$datos['precio'] = $this->input->post('precio');
	$datos['stock'] = $this->input->post('stock');
	$datos['categoria'] = $this->input->post('categoria');
	
	$respuesta = $this->productoM->ingresar($datos);
	$data = array('title'=>'Tienda || producto',
			'producto'=>$this->productoM->get_producto(),
			'categoria'=>$this->productoM->get_categoria(),
			'msj'=>$respuesta);
		$this->load->view('template/header',$data);
		$this->load->view('productoV');
		$this->load->view('template/footer');

	}

		public function get_datos($id){
		$data = array('title'=>'Tienda || producto',
			'datos'=>$this->productoM->get_datos($id),
			'categoria'=>$this->productoM->get_categoria());
		$this->load->view('template/header',$data);
		$this->load->view('productoVA');
		$this->load->view('template/footer');
	}

	public function actualizar(){
	$datos['id_producto'] = $this->input->post('id_producto');
	$datos['nombre_producto'] = $this->input->post('nombre');
	$datos['precio'] = $this->input->post('precio');
	$datos['stock'] = $this->input->post('stock');
	$datos['categoria'] = $this->input->post('categoria');
	
	$respuesta = $this->productoM->actualizar($datos);
	$data = array('title'=>'Tienda || producto',
			'producto'=>$this->productoM->get_producto(),
			'categoria'=>$this->productoM->get_categoria(),
		     'msj'=>$respuesta);
		$this->load->view('template/header',$data);
		$this->load->view('productoV');
		$this->load->view('template/footer');
	}


}
