<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class facturaC extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('facturaM');
	}
	public function index()
	{
		$data = array('title'=>'Tienda || Nueva Factura',
			'factura'=>$this->facturaM->get_factura(),
			'cliente'=>$this->facturaM->get_cliente(),
			'pago'=>$this->facturaM->get_pago());
		$this->load->view('template/header',$data);
		$this->load->view('facturaV');
		$this->load->view('template/footer');
	}

	public function eliminar($id){
		$respuesta = $this->facturaM->eliminar($id);
			$data = array('title'=>'Tienda || Nueva Factura',
			'factura'=>$this->facturaM->get_factura(),
			'cliente'=>$this->facturaM->get_cliente(),
			'pago'=>$this->facturaM->get_pago(),
			'msj'=>$respuesta);
		$this->load->view('template/header',$data);
		$this->load->view('facturaV');
		$this->load->view('template/footer');
		
	}

public function ingresar(){
	$datos['id_cliente'] = $this->input->post('cliente');
	$datos['num_pago'] = $this->input->post('pago');
	$datos['fecha'] = $this->input->post('fecha');

		$respuesta = $this->facturaM->ingresar($datos);
			$data = array('title'=>'Tienda || Nueva Factura',
			'factura'=>$this->facturaM->get_factura(),
			'cliente'=>$this->facturaM->get_cliente(),
			'pago'=>$this->facturaM->get_pago(),
			'msj'=>$respuesta);
		$this->load->view('template/header',$data);
		$this->load->view('facturaV');
		$this->load->view('template/footer');
		
	}
	public function get_datos($id){
			$data = array('title'=>'Tienda || Nueva Factura',
			'datos'=>$this->facturaM->get_datos($id),
			'cliente'=>$this->facturaM->get_cliente(),
			'pago'=>$this->facturaM->get_pago());
		$this->load->view('template/header',$data);
		$this->load->view('facturaVA');
		$this->load->view('template/footer');
		
	}

	

	public function actualizar(){
	$datos['num_factura'] = $this->input->post('num_factura');
	$datos['id_cliente'] = $this->input->post('cliente');
	$datos['num_pago'] = $this->input->post('pago');
	$datos['fecha'] = $this->input->post('fecha');

		$respuesta = $this->facturaM->actualizar($datos);
			$data = array('title'=>'Tienda || Nueva Factura',
			'factura'=>$this->facturaM->get_factura(),
			'cliente'=>$this->facturaM->get_cliente(),
			'pago'=>$this->facturaM->get_pago(),
			'msj'=>$respuesta);
		$this->load->view('template/header',$data);
		$this->load->view('facturaV');
		$this->load->view('template/footer');
		
	}

}
