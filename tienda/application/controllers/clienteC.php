<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class clienteC extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('clienteM');
	}
	public function index()
	{
		$data = array('title'=>'Tienda || cliente',
			'cliente'=>$this->clienteM-> get_cliente());
		$this->load->view('template/header',$data);
		$this->load->view('clienteV');
		$this->load->view('template/footer');
	}

	public function get_factura($id){
		$data = array('title'=>'Tienda || facturas de cliente',
			'factura'=>$this->clienteM->get_factura($id));
		$this->load->view('template/header',$data);
		$this->load->view('clienteVF');
		$this->load->view('template/footer');
	}

	public function eliminar($id){
		$respuesta = $this->clienteM->eliminar($id);
		$data = array('title'=>'Tienda || cliente',
			'cliente'=>$this->clienteM-> get_cliente(),
			'msj'=>$respuesta);
		$this->load->view('template/header',$data);
		$this->load->view('clienteV');
		$this->load->view('template/footer');

	}

	public function ingresar(){
	$datos['nombre'] = $this->input->post('nombre');
	$datos['apellido'] = $this->input->post('apellido');
	$datos['direccion'] = $this->input->post('direccion');
	$datos['fecha'] = $this->input->post('fecha');
	$datos['telefono'] = $this->input->post('telefono');
	$datos['correo'] = $this->input->post('correo');
	
	$respuesta = $this->clienteM->ingresar($datos);
	$data = array('title'=>'Tienda || cliente',
			'cliente'=>$this->clienteM->get_cliente(),
			'msj'=>$respuesta);
		$this->load->view('template/header',$data);
		$this->load->view('clienteV');
		$this->load->view('template/footer');
	}

		public function get_datos($id){
		$data = array('title'=>'Tienda || factura',
			'datos'=>$this->clienteM->get_datos($id));
		$this->load->view('template/header',$data);
		$this->load->view('clienteVA');
		$this->load->view('template/footer');
	}


		public function verFactura($id){
			$data = array('title'=>'Tienda || Detalles Factura',
			'datos'=>$this->clienteM->verFactura($id),
			'cliente'=>$this->clienteM->get_cliente(),
			'pago'=>$this->clienteM->get_pago());
		$this->load->view('template/header',$data);
		$this->load->view('facturaID');
		$this->load->view('template/footer');
		
	}

	public function eliminarF($id){
		$respuesta = $this->clienteM->eliminarF($id);
		$data = array('title'=>'Tienda || Nueva Factura',
			'datos'=>$this->clienteM->verFactura($id),
			'cliente'=>$this->clienteM->get_clientes(),
			'pago'=>$this->clienteM->get_pago(),
			'msj'=>$respuesta);
		$this->load->view('template/header',$data);
		$this->load->view('facturaID');
		$this->load->view('template/footer');
	}


		public function set_compra($id){
		$data = array('title'=>'Tienda || compra',
			'datos'=>$this->clienteM->set_compra($id),
		    'factura'=>$this->clienteM->get_factura($id),
		    'producto'=>$this->clienteM->get_producto());
		$this->load->view('template/header',$data);
		$this->load->view('clienteVC');
		$this->load->view('template/footer');
	}

	public function compra(){
	$datos['id_factura'] = $this->input->post('factura');
	$datos['id_producto'] = $this->input->post('producto');
	$datos['cantidad'] = $this->input->post('cantidad');
	$datos['precio'] = $this->input->post('precio');
	
	$respuesta = $this->clienteM->compra($datos);
	$data = array('title'=>'Tienda || cliente',
			'cliente'=>$this->clienteM->get_cliente(),
			'msj'=>$respuesta);
		$this->load->view('template/header',$data);
		$this->load->view('clienteV');
		$this->load->view('template/footer');
	}

	public function actualizar(){
	$datos['id_cliente'] = $this->input->post('id_cliente');
	$datos['nombre'] = $this->input->post('nombre');
	$datos['apellido'] = $this->input->post('apellido');
	$datos['direccion'] = $this->input->post('direccion');
	$datos['fecha'] = $this->input->post('fecha');
	$datos['telefono'] = $this->input->post('telefono');
	$datos['correo'] = $this->input->post('correo');
	
	$respuesta = $this->clienteM->actualizar($datos);
	$data = array('title'=>'Tienda || cliente',
			'cliente'=>$this->clienteM->get_cliente(),
			'msj'=>$respuesta);
		$this->load->view('template/header',$data);
		$this->load->view('clienteV');
		$this->load->view('template/footer');
	}

}
