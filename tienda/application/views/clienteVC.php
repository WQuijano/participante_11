<body class="container">
	<?php foreach ($datos as $val) { ?>
<div class="container col-md-8" style=" border: black; box-shadow: 3px 5px 10px black; padding: 10px; margin-top: 10%;">
<form action="<?= base_url('clienteC/compra') ?>" method="POST" onsubmit="return validar();">
			<div align="center">
				<label style="color: black; font-family: Lucida Calligraphy; font-size: 100%; font-weight: 900;">Haga una compra</label>
			</div>
		<div class="row">
		
		<div class="col">
			<label>Nombre del cliente</label>
			<input type="text" name="nombre" class="form-control" id="nombre" value="<?= $val->nombre.' '.$val->apellido ?>" readonly>

		    <label>Factura</label>
			<select name="factura" class="form-control" id="factura">
	        	<option value="">Seleccione un numero de factura</option>
           <?php foreach ($factura as $va) { ?>
	        	<option value="<?= $va->num_factura ?>"><?= $va->num_factura ?></option>
	        <?php } ?>
	        </select>
		</div>
		<div class="col">
			<label>Producto</label>
			<select name="producto" class="form-control" id="producto">
	        	<option value="">Seleccione una producto</option>
           <?php foreach ($producto as $va) { ?>
	        	<option value="<?= $va->id_producto ?>"><?= $va->nombre_producto ?></option>
	        <?php } ?>
	        </select>
	
			<label>Cantidad</label>
			<input type="number" name="cantidad" class="form-control" id="cantidad" placeholder="Ingrese el cantidad...">
		</div>
		<div class="col">
			<label>Precio</label>
			<input type="number" step="0.01" name="precio" class="form-control" id="precio" placeholder="Ingrese el precio..."> 

			<input type="submit" name="COMPRAR" value="COMPRAR" class="btn btn-outline-success" style="font-size: 100%; width: 100%; margin-top: 30px">
		</div>
		</div>
	</form>
</div>
	<?php } ?>


	

