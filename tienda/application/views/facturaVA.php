<body class="container">
		<div class="container col-md-8" style=" border: black; ;box-shadow: 3px 5px 10px black; padding: 10px; margin-top: 10%;">
			<?php foreach ($datos as $val) { ?>
	<form action="<?= base_url('facturaC/actualizar') ?>" method="POST" onsubmit="return validar()">
			<div align="center">
				<label style="color: black; font-family: Lucida Calligraphy; font-size: 100%; font-weight: 900;">Actualice la factura</label>
			</div>
		<div class="row">
	<input type="hidden" name="num_factura" value="<?= $val->num_factura ?>">
		<div class="col">
			<label>Cliente</label>
			<select name="cliente" class="form-control" id="cliente">
	        	<option value="">Seleccione un cliente</option>
           <?php foreach ($cliente as $va) { ?>
	        	<option value="<?= $va->id_cliente ?>" <?php if($va->id_cliente==$val->id_cliente){echo "Selected";} ?>><?= $va->nombre.' '.$va->apellido ?></option>
	        <?php } ?>
	        </select>
		
			<label>Fecha</label>
			<input type="date" name="fecha" class="form-control text-centerfacturaC" value="<?= $val->fecha ?>" readonly>
		</div>
		<div class="col">
			<label>Pago</label>
			<select name="pago" class="form-control" id="pago">
	        	<option value="">Seleccione un pago</option>
           <?php foreach ($pago as $va) { ?>
	        	<option value="<?= $va->num_pago ?>" <?php if($va->num_pago==$val->num_pago){echo "Selected";} ?>><?= $va->nombres ?></option>
	        <?php } ?>
	        </select>

	        <input type="submit" name="ACTUALIZAR" value="ACTUALIZAR" class="btn btn-outline-success" style="font-size: 100%; width: 100%; margin-top: 30px">
		</div>
		</div>
	</form>
<?php } ?>
	</div>


	<script type="text/javascript">
	function validar(){
		var cliente = document.getElementById("cliente").selectedIndex;
		var pago = document.getElementById("pago").selectedIndex;

		if(cliente==''){
			document.getElementById("cliente").style.borderColor="red";
			document.getElementById("cliente").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("cliente").style.borderColor="green";
			document.getElementById("cliente").style.boxShadow="3px 4px 5px green";
		}

		if(pago==''){
			document.getElementById("pago").style.borderColor="red";
			document.getElementById("pago").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("pago").style.borderColor="green";
			document.getElementById("pago").style.boxShadow="3px 4px 5px green";
		}
		return true;
	}
</script>