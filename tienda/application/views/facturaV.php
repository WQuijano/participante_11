
<nav style="font-size: 200%;background-color: black; ">
		<nav>
			<ul>
				<label style="margin-right: 60%; color: white; font-family: Lucida Calligraphy; font-size: 100%; font-weight: 900;width: 5%">Tienda</label>
			<i>
				<a href="<?= base_url('facturaC/index') ?>"><button class="btn btn-success">FACTURAS</button></a>
			</i>
			<i>
					<a href="<?= base_url('productoC/index') ?>"><button class="btn btn-success">PRODUCTOS</button></a>
				</i>
				<i>
						<a href="<?= base_url('clienteC/index') ?>"><button class="btn btn-success">CLIENTES</button></a>
				</i>
			</ul>
		</nav>
	</nav>
<body class="container">
		<div class="container col-md-8" style=" border: black; ;box-shadow: 3px 5px 10px black; padding: 10px">
	<form action="<?= base_url('facturaC/ingresar') ?>" method="POST" onsubmit="return validar();">
			<div align="center">
				<label style="color: black; font-family: Lucida Calligraphy; font-size: 100%; font-weight: 900;">Ingrese una factura</label>
			</div>
		<div class="row">
		
		<div class="col">
			<label>Cliente</label>
			<select name="cliente" class="form-control" id="cliente">
	        	<option value="">Seleccione un cliente</option>
           <?php foreach ($cliente as $va) { ?>
	        	<option value="<?= $va->id_cliente ?>"><?= $va->nombre.' '.$va->apellido ?></option>
	        <?php } ?>
	        </select>
		
			<label>Fecha</label>
			<input type="date" name="fecha" class="form-control text-centerfacturaC" value="<?= date('Y-m-d') ?>" readonly>
		</div>
		<div class="col">
			<label>Pago</label>
			<select name="pago" class="form-control" id="pago">
	        	<option value="">Seleccione un pago</option>
           <?php foreach ($pago as $va) { ?>
	        	<option value="<?= $va->num_pago ?>"><?= $va->nombres ?></option>
	        <?php } ?>
	        </select>

	        <input type="submit" name="INGRESAR" value="INGRESAR" class="btn btn-outline-success" style="font-size: 100%; width: 100%; margin-top: 30px">
		</div>
		</div>
	</form>
	</div>

	<div align="center">
		<label style="color: black; font-family: Lucida Calligraphy; font-size: 200%; font-weight: 900;">Facturas</label>
	</div>
	<table border="1" class="table table-dark table-hover">
	<thead>
		<tr>
			<td>N° de Factura</td>
			<td>Nombre del cliente</td>
			<td>Fecha</td>
			<td>Pago</td>
			<td>Eliminar</td>
			<td>Editar</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($factura as $va) { ?>
		<tr>
			<td><?= $va->num_factura ?></td>
			<td><?= $va->nombre.' '.$va->apellido ?></td>
			<td><?= $va->fecha ?></td>
			<td><?= $va->nombres ?></td>
			<td align="center"><a href="<?= base_url('facturaC/eliminar/').$va->num_factura ?>" onclick="confirm('Esta seguro de eliminar este producto?')"><button class="btn btn-danger">Eliminar</button></a></td>
			<td align="center"><a href="<?= base_url('facturaC/get_datos/').$va->num_factura ?>"><button class="btn btn-primary">Editar</button></a></td>
		</tr>
	<?php } ?>
	</tbody>
</table>


<?php if(isset($msj)){
	if($msj=='eli'){
	header('refresh: 0; url=http://localhost/tienda/facturaC') ?>
	<script>
		alert('Eliminado exitosamente');
	</script>
<?php } if($msj=='add'){
	header('refresh: 0; url=http://localhost/tienda/facturaC') ?>
	<script>
		alert('Ingresado exitosamente');
	</script>
<?php } if($msj=='edi'){
	header('refresh: 0; url=http://localhost/tienda/facturaC') ?>
	<script>
		alert('Actualizado exitosamente');
	</script>
<?php } if($msj==false){
	header('refresh: 0; url=http://localhost/tienda/facturaC') ?>
	<script>
		alert('Error ocurrio un conflicto!!!');
	</script>
<?php } } ?>

<script type="text/javascript">
	function validar(){
		var cliente = document.getElementById("cliente").selectedIndex;
		var pago = document.getElementById("pago").selectedIndex;

		if(cliente==''){
			document.getElementById("cliente").style.borderColor="red";
			document.getElementById("cliente").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("cliente").style.borderColor="green";
			document.getElementById("cliente").style.boxShadow="3px 4px 5px green";
		}

		if(pago==''){
			document.getElementById("pago").style.borderColor="red";
			document.getElementById("pago").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("pago").style.borderColor="green";
			document.getElementById("pago").style.boxShadow="3px 4px 5px green";
		}
		return true;
	}
</script>