
	<nav style="font-size: 200%;background-color: black; ">
		<nav>
			<ul>
				<label style="margin-right: 60%; color: white; font-family: Lucida Calligraphy; font-size: 100%; font-weight: 900;width: 5%">Tienda</label>
			<i>
				<a href="<?= base_url('facturaC/index') ?>"><button class="btn btn-success">FACTURAS</button></a>
			</i>
			<i>
					<a href="<?= base_url('productoC/index') ?>"><button class="btn btn-success">PRODUCTOS</button></a>
				</i>
				<i>
						<a href="<?= base_url('clienteC/index') ?>"><button class="btn btn-success">CLIENTES</button></a>
				</i>
			</ul>
		</nav>
	</nav>

<body class="container">
<div class="container col-md-8" style=" border: black; box-shadow: 3px 5px 10px black; padding: 10px">
	<form action="<?= base_url('clienteC/ingresar') ?>" method="POST" onsubmit="return validar();">
			<div align="center">
				<label style="color: black; font-family: Lucida Calligraphy; font-size: 100%; font-weight: 900;">Ingrese un cliente</label>
			</div>
		<div class="row">
		
		<div class="col">
			<label>Nombre</label>
			<input type="text" name="nombre" class="form-control" id="nombre" placeholder="Ingrese el nombre del cliente...">
		
			<label>Apellido</label>
			<input type="text" name="apellido" class="form-control" id="apellido" placeholder="Ingrese el apellido del cliente...">
		</div>
		<div class="col">
		     <label>Direccion</label>
		     <input type="text" name="direccion" class="form-control" id="direccion" placeholder="Ingrese la direccion del cliente...">
	
			<label>Fecha de nacimiento</label>
			<input type="date" name="fecha" class="form-control" id="fecha">
		</div>
		<div class="col">
			<label>Telefono</label>
			<input type="number" name="telefono" class="form-control" id="telefono" placeholder="Ingrese el telefono del cliente...">
	
			<label>Correo</label>
			<input type="text" name="correo" class="form-control" id="correo" placeholder="Ingrese el correo del cliente...">
		</div>
		<div class="container">
			<input type="submit" name="INGRESAR" value="INGRESAR" class="btn btn-outline-success" style="font-size: 100%; width: 100%; height: 80%; margin-top: 10px">
		</div>
		</div>
	</form>
	</div>
	<div align="center">
		<label style="color: black; font-family: Lucida Calligraphy; font-size: 200%; font-weight: 900;">Clientes</label>
	</div>
<table id="cliente" border="1" class="table table-dark table-hover" style="margin-top: 10px; font-size: 100%;">
	<thead>
		<tr>
			<td>N°</td>
			<td>Nombre</td>
			<td>Apellido</td>
			<td>Direccion</td>
			<td>Fecha de nacimiento</td>
			<td>Telefono</td>
			<td>Email</td>
			<td>Factura/Compra</td>
			<td>Eliminar</td>
			<td>Editar</td>
		</tr>
	</thead>
	<tbody>
		<?php $n=1; foreach ($cliente as $va) { ?>
		<tr>
			<td><?= $n; ?></td>
			<td><?= $va->nombre ?></td>
			<td><?= $va->apellido ?></td>
			<td><?= $va->direccion ?></td>
			<td><?= $va->fecha_nacimiento ?></td>
			<td><?= $va->telefono ?></td>
			<td><?= $va->email ?></td>
		
			<td align="center"><a href="<?= base_url('clienteC/get_factura/').$va->id_cliente ?>"><button class="btn btn-primary">Facturas</button></a>
				<a href="<?= base_url('clienteC/set_compra/').$va->id_cliente ?>"><button class="btn btn-success">Compra</button></a></td>
			<td align="center"><a href="<?= base_url('clienteC/eliminar/').$va->id_cliente ?>" onclick="confirm('Esta seguro de eliminar este cliente?')"><button class="btn btn-danger">Eliminar</button></a></td>
			<td align="center"><a href="<?= base_url('clienteC/get_datos/').$va->id_cliente ?>"><button class="btn btn-primary">Editar</button></a></td>
		</tr>
	<?php $n++; } ?>
	</tbody>
</table>

<?php if(isset($msj)){
	if($msj=='eli'){
	header('refresh: 0; url=http://localhost/tienda/') ?>
	<script>
		alert('Eliminado exitosamente');
	</script>
<?php } if($msj=='add'){
	header('refresh: 0; url=http://localhost/tienda/') ?>
	<script>
		alert('Ingresado exitosamente');
	</script>
<?php } if($msj=='compra'){
	header('refresh: 0; url=http://localhost/tienda/') ?>
	<script>
		alert('La compra se realizo correctamente');
	</script>
<?php } if($msj=='edi'){
	header('refresh: 0; url=http://localhost/tienda/') ?>
	<script>
		alert('Actualizado exitosamente');
	</script>
<?php } if($msj==false){
	header('refresh: 0; url=http://localhost/tienda/') ?>
	<script>
		alert('Error ocurrio un conflicto!!!');
	</script>
<?php } } ?>

<script type="text/javascript">
	function validar(){
		var nombre = document.getElementById("nombre").value;
		var apellido = document.getElementById("apellido").value;
		var direccion = document.getElementById("direccion").value;
		var fecha = document.getElementById("fecha").value;
		var telefono = document.getElementById("telefono").value;
		var correo = document.getElementById("correo").value;

		if(nombre.length==0 || nombre.length>30){
			document.getElementById("nombre").style.borderColor="red";
			document.getElementById("nombre").placeholder="Campo obligatorio";
			document.getElementById("nombre").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("nombre").style.borderColor="green";
			document.getElementById("nombre").style.boxShadow="3px 4px 5px green";
		}

		if(apellido.length==0 || apellido.length>30){
			document.getElementById("apellido").style.borderColor="red";
			document.getElementById("apellido").placeholder="Campo obligatorio";
			document.getElementById("apellido").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("apellido").style.borderColor="green";
			document.getElementById("apellido").style.boxShadow="3px 4px 5px green";
		}

		if(direccion.length==0 || direccion.length>75){
			document.getElementById("direccion").style.borderColor="red";
			document.getElementById("direccion").placeholder="Campo obligatorio";
			document.getElementById("direccion").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("direccion").style.borderColor="green";
			document.getElementById("direccion").style.boxShadow="3px 4px 5px green";
		}

		if(fecha.length==''){
			document.getElementById("fecha").style.borderColor="red";
			document.getElementById("fecha").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("fecha").style.borderColor="green";
			document.getElementById("fecha").style.boxShadow="3px 4px 5px green";
		}

		if(telefono.length=='' || telefono.length>11){
			document.getElementById("telefono").style.borderColor="red";
			document.getElementById("telefono").placeholder="Campo obligatorio";
			document.getElementById("telefono").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("telefono").style.borderColor="green";
			document.getElementById("telefono").style.boxShadow="3px 4px 5px green";
		}

		if(correo.length=='' || correo.length>75){
			document.getElementById("correo").style.borderColor="red";
			document.getElementById("correo").placeholder="Campo obligatorio";
			document.getElementById("correo").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("correo").style.borderColor="green";
			document.getElementById("correo").style.boxShadow="3px 4px 5px green";
		}
		return true;
	}
</script>


