<nav style="font-size: 200%;background-color: black; ">
		<nav>
			<ul>
				<label style="margin-right: 60%; color: white; font-family: Lucida Calligraphy; font-size: 100%; font-weight: 900;width: 5%">Tienda</label>
			<i>
				<a href="<?= base_url('facturaC/index') ?>"><button class="btn btn-success">FACTURAS</button></a>
			</i>
			<i>
					<a href="<?= base_url('productoC/index') ?>"><button class="btn btn-success">PRODUCTOS</button></a>
				</i>
				<i>
						<a href="<?= base_url('clienteC/index') ?>"><button class="btn btn-success">CLIENTES</button></a>
				</i>
			</ul>
		</nav>
	</nav>
<body class="container">
	<div align="center">
		<label style="color: black; font-family: Lucida Calligraphy; font-size: 200%; font-weight: 900;">Facturas de cliente</label>
	</div>
<table border="1" class="table table-dark table-hover">
	<thead>
		<tr>
			<td>N° de factura</td>
			<td>Nombre</td>
			<td>Apellido</td>
			<td>Fecha</td>
			<td>Pago</td>
			<td>Detalles</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($factura as $va) { ?>
		<tr>
			<td><?= $va->num_factura ?></td>
			<td><?= $va->nombre ?></td>
			<td><?= $va->apellido ?></td>
			<td><?= $va->fecha ?></td>
			<td><?= $va->nombres ?></td>
			<td align="center"><a href="<?= base_url('clienteC/verFactura/').$va->num_factura ?>"><button class="btn btn-primary">Ver detalles</button></a></td>
		</tr>
	<?php } ?>
	</tbody>
</table>