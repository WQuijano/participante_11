<nav style="font-size: 200%;background-color: black; ">
		<nav>
			<ul>
				<label style="margin-right: 60%; color: white; font-family: Lucida Calligraphy; font-size: 100%; font-weight: 900;width: 5%">Tienda</label>
			<i>
				<a href="<?= base_url('facturaC/index') ?>"><button class="btn btn-success">FACTURAS</button></a>
			</i>
			<i>
					<a href="<?= base_url('productoC/index') ?>"><button class="btn btn-success">PRODUCTOS</button></a>
				</i>
				<i>
						<a href="<?= base_url('clienteC/index') ?>"><button class="btn btn-success">CLIENTES</button></a>
				</i>
			</ul>
		</nav>
	</nav>
<body class="container">
	<div class="container col-md-8" style=" border: black; ;box-shadow: 3px 5px 10px black; padding: 10px;">
	<form action="<?= base_url('productoC/ingresar') ?>" method="POST" onsubmit="return validar();">
			<div align="center">
				<label style="color: black; font-family: Lucida Calligraphy; font-size: 100%; font-weight: 900;">Ingrese un producto</label>
			</div>
		<div class="row">
		
		<div class="col">
			<label>Nombre</label>
			<input type="text" name="nombre" class="form-control" placeholder="Ingrese el nombre del producto..." id="nombre">
		
			<label>Precio</label>
			<input type="number" step="0.01" name="precio" class="form-control" placeholder="Ingrese el precio del producto..." id="precio">
		</div>
		<div class="col">
		     <label>Stock</label>
		     <input type="number" name="stock" class="form-control" placeholder="Ingrese el stock del producto..." id="stock">
	
			<label>Categoria</label>
			<select name="categoria" class="form-control" id="categoria">
	        	<option value="">Seleccione una categoria</option>
           <?php foreach ($categoria as $va) { ?>
	        	<option value="<?= $va->id_categoria ?>"><?= $va->nombres ?></option>
	        <?php } ?>
	        </select>
		</div>
		
			
          <div class="container">
			<input type="submit" name="INGRESAR" value="INGRESAR" class="btn btn-outline-success" style="font-size: 100%; width: 100%; height: 80%; margin-top: 10px">
		</div>
		</div>
	</form>
	</div>
	<div align="center">
		<label style="color: black; font-family: Lucida Calligraphy; font-size: 200%; font-weight: 900;">Productos</label>
	</div>
<table border="1" class="table table-dark table-hover">
	<thead>
		<tr>
			<td>N° de producto</td>
			<td>Nombre de producto</td>
			<td>Precio</td>
			<td>stock</td>
			<td>Categoria</td>
			<td>Eliminar</td>
			<td>Editar</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($producto as $va) { ?>
		<tr>
			<td><?= $va->id_producto ?></td>
			<td><?= $va->nombre_producto ?></td>
			<td><?= $va->precio ?></td>
			<td><?= $va->stock ?></td>
			<td><?= $va->nombres ?></td>
			<td align="center"><a href="<?= base_url('productoC/eliminar/').$va->id_producto ?>" onclick="confirm('Esta seguro de eliminar este producto?')"><button class="btn btn-danger">Eliminar</button></a></td>
			<td align="center"><a href="<?= base_url('productoC/get_datos/').$va->id_producto ?>"><button class="btn btn-primary">Editar</button></a></td>
		</tr>
	<?php } ?>
	</tbody>
</table>


<?php if(isset($msj)){
	if($msj=='eli'){
	header('refresh: 0; url=http://localhost/tienda/productoC') ?>
	<script>
		alert('Eliminado exitosamente');
	</script>
<?php } if($msj=='add'){
	header('refresh: 0; url=http://localhost/tienda/productoC') ?>
	<script>
		alert('Ingresado exitosamente');
	</script>
<?php } if($msj=='edi'){
	header('refresh: 0; url=http://localhost/tienda/productoC') ?>
	<script>
		alert('Actualizado exitosamente');
	</script>
<?php } if($msj==false){
	header('refresh: 0; url=http://localhost/tienda/productoC') ?>
	<script>
		alert('Error ocurrio un conflicto!!!');
	</script>
<?php } } ?>

<script>
function validar(){
		var nombre = document.getElementById("nombre").value;
		var precio = document.getElementById("precio").value;
		var stock = document.getElementById("stock").value;
		var categoria = document.getElementById("categoria").selectedIndex;
		

		if(nombre.length==0 || nombre.length>30){
			document.getElementById("nombre").style.borderColor="red";
			document.getElementById("nombre").placeholder="Campo obligatorio";
			document.getElementById("nombre").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("nombre").style.borderColor="green";
			document.getElementById("nombre").style.boxShadow="3px 4px 5px green";
		}

		if(precio.length==''){
			document.getElementById("precio").style.borderColor="red";
			document.getElementById("precio").placeholder="Campo obligatorio";
			document.getElementById("precio").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("precio").style.borderColor="green";
			document.getElementById("precio").style.boxShadow="3px 4px 5px green";
		}


		if(stock.length==''){
			document.getElementById("stock").style.borderColor="red";
			document.getElementById("stock").placeholder="Campo obligatorio";
			document.getElementById("stock").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("stock").style.borderColor="green";
			document.getElementById("stock").style.boxShadow="3px 4px 5px green";
		}

		if(categoria==''){
			document.getElementById("categoria").style.borderColor="red";
			document.getElementById("categoria").placeholder="Campo obligatorio";
			document.getElementById("categoria").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("categoria").style.borderColor="green";
			document.getElementById("categoria").style.boxShadow="3px 4px 5px green";
		}

		
		return true;
	}
</script>


