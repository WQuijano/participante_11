
<body class="container">
		<?php foreach ($datos as $key => $val) { ?>
<div class="container col-md-8" style=" border: black; ;box-shadow: 3px 5px 10px black; padding: 10px; margin-top: 10%;">
	<form action="<?= base_url('clienteC/actualizar') ?>" method="POST" onsubmit="return validar();">
			<div align="center">
				<label style="color: black; font-family: Lucida Calligraphy; font-size: 100%; font-weight: 900;">Actualice un cliente</label>
			</div>
		<div class="row">
		<input type="hidden" name="id_cliente" value="<?= $val->id_cliente ?>">
		<div class="col">
			<label>Nombre</label>
			<input type="text" name="nombre" class="form-control" value="<?= $val->nombre ?>" id="nombre">
		
			<label>Apellido</label>
			<input type="text" name="apellido" class="form-control" value="<?= $val->apellido ?>" id="apellido">
		</div>
		<div class="col">
		     <label>Direccion</label>
		     <input type="text" name="direccion" class="form-control" value="<?= $val->direccion ?>" id="direccion">
	
			<label>Fecha de nacimiento</label>
			<input type="date" name="fecha" class="form-control" value="<?= $val->fecha_nacimiento ?>" id="fecha">
		</div>
		<div class="col">
			<label>Telefono</label>
			<input type="number" name="telefono" class="form-control" value="<?= $val->telefono ?>" id="telefono">
	
			<label>Correo</label>
			<input type="text" name="correo" class="form-control" value="<?= $val->email ?>" id="correo">
		</div>
		<div class="container">
			<input type="submit" name="ACTUALIZAR" value="ACTUALIZAR" class="btn btn-outline-success" style="font-size: 100%; width: 100%; height: 80%; margin-top: 10px">
		</div>
		</div>
	</form>

	</div>
	<?php	} ?>

	<script type="text/javascript">
	function validar(){
		var nombre = document.getElementById("nombre").value;
		var apellido = document.getElementById("apellido").value;
		var direccion = document.getElementById("direccion").value;
		var fecha = document.getElementById("fecha").value;
		var telefono = document.getElementById("telefono").value;
		var correo = document.getElementById("correo").value;

		if(nombre.length==0 || nombre.length>30){
			document.getElementById("nombre").style.borderColor="red";
			document.getElementById("nombre").placeholder="Campo obligatorio";
			document.getElementById("nombre").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("nombre").style.borderColor="green";
			document.getElementById("nombre").style.boxShadow="3px 4px 5px green";
		}

		if(apellido.length==0 || apellido.length>30){
			document.getElementById("apellido").style.borderColor="red";
			document.getElementById("nombre").placeholder="Campo obligatorio";
			document.getElementById("apellido").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("apellido").style.borderColor="green";
			document.getElementById("apellido").style.boxShadow="3px 4px 5px green";
		}

		if(direccion.length==0 || direccion.length>75){
			document.getElementById("direccion").style.borderColor="red";
			document.getElementById("direccion").placeholder="Campo obligatorio";
			document.getElementById("direccion").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("direccion").style.borderColor="green";
			document.getElementById("direccion").style.boxShadow="3px 4px 5px green";
		}

		if(fecha.length==''){
			document.getElementById("fecha").style.borderColor="red";
			document.getElementById("fecha").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("fecha").style.borderColor="green";
			document.getElementById("fecha").style.boxShadow="3px 4px 5px green";
		}

		if(telefono.length=='' || telefono.length>11){
			document.getElementById("telefono").style.borderColor="red";
			document.getElementById("telefono").placeholder="Campo obligatorio";
			document.getElementById("telefono").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("telefono").style.borderColor="green";
			document.getElementById("telefono").style.boxShadow="3px 4px 5px green";
		}

		if(correo.length=='' || correo.length>75){
			document.getElementById("correo").style.borderColor="red";
			document.getElementById("correo").placeholder="Campo obligatorio";
			document.getElementById("correo").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("correo").style.borderColor="green";
			document.getElementById("correo").style.boxShadow="3px 4px 5px green";
		}
		return true;
	}
</script>


