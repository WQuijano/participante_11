<body class="container">
	<div class="container col-md-8" style=" border: black; box-shadow: 3px 5px 10px black; padding: 10px; margin-top: 10%;">
		<?php foreach ($datos as $val) { ?>
	<form action="<?= base_url('productoC/actualizar') ?>" method="POST" onsubmit="return validar();">
			<div align="center">
				<label style="color: black; font-family: Lucida Calligraphy; font-size: 100%; font-weight: 900;">Ingrese un producto</label>
			</div>
			<input type="hidden" name="id_producto" value="<?= $val->id_producto ?>">
		<div class="row">
		<div class="col">
			<label>Nombre</label>
			<input type="text" name="nombre" class="form-control" value="<?= $val->nombre_producto ?>" id="nombre">
		
			<label>Precio</label>
			<input type="number" step="0.01" name="precio" class="form-control" value="<?= $val->precio ?>" id="precio">
		</div>
		<div class="col">
		     <label>Stock</label>
		     <input type="number" name="stock" class="form-control" value="<?= $val->stock ?>" id="stock">
	
			<label>Categoria</label>
			<select name="categoria" class="form-control" id="categoria">
	        	<option value="">Seleccione una categoria</option>
           <?php foreach ($categoria as $va) { ?>
	        	<option value="<?= $va->id_categoria ?>" <?php if($va->id_categoria==$val->id_categoria){echo "Selected";} ?>><?= $va->nombres ?></option>
	        <?php } ?>
	        </select>
		</div>
		      <div class="container">
			<input type="submit" name="ACTUALIZAR" value="ACTUALIZAR" class="btn btn-outline-success" style="font-size: 100%; width: 100%; height: 80%; margin-top: 10px">
		</div>
		</div>
	</form>
<?php } ?>
	</div>

	<script>
function validar(){
		var nombre = document.getElementById("nombre").value;
		var precio = document.getElementById("precio").value;
		var stock = document.getElementById("stock").value;
		var categoria = document.getElementById("categoria").selectedIndex;
		

		if(nombre.length==0 || nombre.length>30){
			document.getElementById("nombre").style.borderColor="red";
			document.getElementById("nombre").placeholder="Campo obligatorio";
			document.getElementById("nombre").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("nombre").style.borderColor="green";
			document.getElementById("nombre").style.boxShadow="3px 4px 5px green";
		}

		if(precio.length==''){
			document.getElementById("precio").style.borderColor="red";
			document.getElementById("precio").placeholder="Campo obligatorio";
			document.getElementById("precio").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("precio").style.borderColor="green";
			document.getElementById("precio").style.boxShadow="3px 4px 5px green";
		}


		if(stock.length==''){
			document.getElementById("stock").style.borderColor="red";
			document.getElementById("stock").placeholder="Campo obligatorio";
			document.getElementById("stock").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("stock").style.borderColor="green";
			document.getElementById("stock").style.boxShadow="3px 4px 5px green";
		}

		if(categoria==''){
			document.getElementById("categoria").style.borderColor="red";
			document.getElementById("categoria").placeholder="Campo obligatorio";
			document.getElementById("categoria").style.boxShadow="3px 4px 5px red";
			return false;
		}else{
			document.getElementById("categoria").style.borderColor="green";
			document.getElementById("categoria").style.boxShadow="3px 4px 5px green";
		}

		
		return true;
	}
</script>