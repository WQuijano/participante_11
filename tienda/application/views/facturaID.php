<nav style="font-size: 200%;background-color: black; ">
		<nav>
			<ul>
				<label style="margin-right: 60%; color: white; font-family: Lucida Calligraphy; font-size: 100%; font-weight: 900;width: 5%">Tienda</label>
			<i>
				<a href="<?= base_url('facturaC/index') ?>"><button class="btn btn-success">FACTURAS</button></a>
			</i>
			<i>
					<a href="<?= base_url('productoC/index') ?>"><button class="btn btn-success">PRODUCTOS</button></a>
				</i>
				<i>
						<a href="<?= base_url('clienteC/index') ?>"><button class="btn btn-success">CLIENTES</button></a>
				</i>
			</ul>
		</nav>
	</nav>
<body class="container">
	<div align="center">
		<label style="color: black; font-family: Lucida Calligraphy; font-size: 200%; font-weight: 900;">Detalles de Factura</label>
	</div>
<table border="1" class="table table-hover">
	<thead>
		<tr>
			<td>Nombre de producto</td>
			<td>Nombre del cliente</td>
			<td>N° de factura</td>
			<td>Fecha</td>
			<td>Cantidad</td>
			<td>Precio</td>
			<td>Pago</td>
			<td>Eliminar</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($datos as $va) { ?>
		<tr>
			<td><?= $va->nombre_producto ?></td>
			<td><?= $va->nombre.' '.$va->apellido ?></td>
			<td><?= $va->num_factura ?></td>
			<td><?= $va->fecha ?></td>
			<td><?= $va->cantidad ?></td>
			<td><?= $va->precio ?></td>
			<td><?= $va->nombres ?></td>
			<td align="center"><a href="<?= base_url('clienteC/eliminarF/').$va->num_detalle ?>" onclick="confirm('Esta seguro de eliminar este detalle de factura??')"><button class="btn btn-danger">Eliminar</button></a></td>
		</tr>
	<?php } ?>
	</tbody>
</table>

<?php if(isset($msj)){
	if($msj=='elim'){
	header('refresh: 0; url=http://localhost/tienda/') ?>
	<script>
		alert('Eliminado exitosamente');
	</script>
<?php } } ?>