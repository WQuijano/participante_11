<?php 

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Entities\Usuario;
use App\Models\UsuarioImp;

class UsuarioC extends BaseController
{

	public function index(){

    $u = new UsuarioImp();

    $data['usuarios'] = $u->getUsuario();
    $data['empleado'] = $u->get_Empleado();
    $data['rol'] = $u->get_Rol();


    $data['title'] = '|| Usuario || ';

    echo view('templates/header', $data);
    echo view('UsuarioView');
    echo view('templates/footer');
  }


  public function delete($id)
	{
		$usuario = new UsuarioImp();

		$msj = $usuario->eliminar($id);

		if ($msj == true) {
			echo "<script>alert('Eliminado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Eliminar!!!')</script>";
		}

		return redirect()->to('/pr1_vemue/public/UsuarioC');
	}

	public function insertar()
  {
    $usuario = new UsuarioImp();

    // Create
    $user = new Usuario();
    $user->setUsu($this->request->getPost('usuario'));
    $user->setCla($this->request->getPost('pass'));
    $user->setEmpl($this->request->getPost('empleado'));
    $user->setRol($this->request->getPost('rol'));

    $msj = $usuario->insertar($user);

    if ($msj == "add") {
      echo "<script>alert('Agregado Exitosamente!!!!!')</script>";
    }else{
      echo "<script>alert('Error al Agregar!!!')</script>";
    }

    return redirect()->to('/pr1_vemue/public/UsuarioC');
  }

  public function getDatos($id)
  {
    $u = new UsuarioImp();

     $data['datos'] = $u->get_Datos($id);
    $data['empleado'] = $u->get_Empleado();
    $data['rol'] = $u->get_Rol();
    $data['title'] = '|| Usuario || ';

    echo view('templates/header', $data);
    echo view('editUsuario');
    echo view('templates/footer');
  }

  //--------------------------------------------------------------------

  public function actualizar()
  {
    $usuario = new UsuarioImp();

    // Create
    $user = new Usuario();
    $user->setID($this->request->getPost('id'));
    $user->setUsu($this->request->getPost('usuario'));
    $user->setEmpl($this->request->getPost('empleado'));
    $user->setRol($this->request->getPost('rol'));

    $msj = $usuario->actualizar($user);

    if ($msj == true) {
      echo "<script>alert('Modificado Exitosamente!!!!!')</script>";
    }else{
      echo "<script>alert('Error al Modificar!!!')</script>";
    }

    return redirect()->to('/pr1_vemue/public/UsuarioC');
  }
	



}