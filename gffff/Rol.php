<?php 
namespace App\Entities;
use CodeIgniter\Entity;

class Rol extends Entity
{

	private $id_rol;
	private $rol;
	

	public function getID(){
		return $this->id_rol;
	}

	public function setID($id){
		$this->id_rol = $id;
	}

	public function getRol(){
		return $this->rol;
	}

	public function setRol($rol){
		$this->rol = $rol;
	}


	
	
}
 ?>