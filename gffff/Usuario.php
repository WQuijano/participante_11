<?php 
namespace App\Entities;
use CodeIgniter\Entity;

class Usuario extends Entity
{

	private $id_usuario;
	private $usuario;
	private $clave;
	private $id_empleado;
	private $id_rol;


	public function getID(){
		return $this->id_usuario;
	} 

	public function setID($id_usuario){
		$this->id_usuario = $id_usuario;
	}
	public function getUsu(){
		return $this->usuario;
	} 

	public function setUsu($usu){
		$this->usuario = $usu;
	}

	public function getCla(){
		return $this->clave;
	} 

	public function setCla($cla){
		$this->clave = $cla;
	}

	public function getEmpl(){
		return $this->id_empleado;
	} 

	public function setEmpl($emp){
		$this->id_empleado = $emp;
	}
	

	public function getRol(){
		return $this->id_rol;
	} 

	public function setRol($rol){
		$this->id_rol = $rol;
	}
	
	

}
 ?>