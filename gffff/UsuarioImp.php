<?php namespace App\Models;

use CodeIgniter\Model;
use App\Entities\Usuario;
use App\Entities\Empleado;
use App\Entities\Rol;

class UsuarioImp extends Model
{ 
    protected $table      = 'usuario';
    protected $primaryKey = 'id_usuario';

    protected $returnType = 'App\Entities\User';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['id_usuario', 'usuario', 'clave', 'id_empleado', 'id_rol'];

    public $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;

   

  public function getUsuario(){
    $mos = 'CALL sp_mostrarUsu()';
    $query = $this->query($mos);

    $usu = array();

    foreach ($query->getResult() as $u) {
      $x = new Usuario();
      $x->setID($u->id_usuario);
      $x->setUsu($u->usuario);
      $x->setEmpl($u->nombre." ".$u->apellido);
      $x->setRol($u->rol);
      array_push($usu, $x);
    }

    return $usu;
  }


  public function eliminar($id)
  {
    $eli = 'CALL  sp_eliminarUsu(?)';
    $query = $this->query($eli, [$id]);

    if ($query) {
      return true;
    }else{
      return false;
    }
  }

  public function get_Empleado()
  {
    $sp_empleado = 'CALL  sp_empleado()';
    $query = $this->query($sp_empleado);


    $empleado = array();

    foreach ($query->getResult() as $row) {
      $x = new Empleado();
      $x->setID($row->id_empleado);
      $x->setNom($row->nombre);
      $x->setAp($row->apellido);      
      array_push($empleado, $x);
    }

    return $empleado;
  }


  public function get_Rol()
  {
    $sp_rol = 'CALL sp_rol()';
    $query = $this->query($sp_rol);


    $rol = array();

    foreach ($query->getResult() as $row) {
      $x = new Rol();
      $x->setID($row->id_rol);
      $x->setRol($row->rol);   
      array_push($rol, $x);
    }

    return $rol;
  }


  public function insertar($user)
  {
    $sp_insertar_usuario = 'CALL  sp_insertarUsu(?, ?, ?, ?)';
    $query = $this->query($sp_insertar_usuario, [$user->getUsu(), $user->getCla(), $user->getEmpl(), $user->getRol()]);

    if ($query) {
      return "add";
    }else{
      return "errorA";
    }
  }


  public function get_Datos($id)
  {
    $sp_id = 'CALL  sp_userId(?)';
    $query = $this->query($sp_id, [$id]);

    if ($query) {
      foreach ($query->getResult() as $row)
        {
            // Create
          $user = new Usuario();
          $user->setID($row->id_usuario);
          $user->setUsu($row->usuario);
          $user->setEmpl($row->id_empleado);
          $user->setRol($row->id_rol);
         
          return $user;
        }
    }else{
      return false;
    }
  }

  //--------------------------------------------------------------------

  public function actualizar($user)
  {
    $sp_modificar_usuario = 'CALL sp_actualizarUsu(?, ?, ?,?)';
    $query = $this->query($sp_modificar_usuario, [$user->getID() ,$user->getUsu(),$user->getEmpl() ,$user->getRol()]);

    if ($query) {
      return "modi";
    }else{
      return "errorM";
    }
  }
        

	//--------------------------------------------------------------------

}
