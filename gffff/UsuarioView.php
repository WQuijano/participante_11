 <?php echo view('templates/navbar'); ?>
<body>
 

	 <button type="button" class="btn btn-outline-success" id="btnM" style="margin-top: 10px; margin-left: 15px;">Nuevo Usuario</button>

	 <div class="col-12 tm-block-col">
    <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
      <h2 class="tm-block-title">FACTURAS</h2>
	<table class="table">
		<thead>
		<tr>
			<th>ID</th>
			<th>Usuario</th>
			<th>Empleado</th>
			<th>Rol</th>
			<th>Eliminar</th>
      <th>Actualizar</th>

		</tr>
		</thead>
		<tbody>
		<?php $n=1;  foreach($usuarios as $u){ ?>
		<tr>
			<td><?= $n++ ?></td>
			<td><?= $u->usu ?></td>
			<td><?= $u->empl ?></td>
			<td><?= $u->rol ?></td>
			<td><a class="btn btn-danger" onclick="return confirm('Desea realmente eliminar este Usuario?')" href="UsuarioC/delete/<?php echo $u->id_usuario;  ?>">Eliminar</a></td>
      <td><a class="btn btn-info" href="UsuarioC/getDatos/<?php echo $u->id_usuario;  ?>">Actualizar</a></td>

		
		</tr>
		<?php } ?>
		</tbody>
	</table>
</div>
</div>



<div class="modal" tabindex="-1" role="dialog" id="modalGuardar">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Nuevo Usuario</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="UsuarioC/insertar" method="POST">
            <div>
              <label>Usuario</label>
              <input type="text" name="usuario" id="usuario" class="form-control">
            </div>
            <div>
              <label>clave</label>
              <input type="password" name="pass" id="pass" value="" class="form-control" >
            </div>
            <div>
              <label>Empleado</label>
              <select name="empleado" id="empleado" class="form-control">
                <option value="">Seleccione Empleado</option>
                	<?php foreach ($empleado as $e) { ?>
                		<option value="<?= $e->id ?>"><?php echo $e->nom ." ".$e->ap ?></option>
                	<?php } ?>
                  
                
              </select>
            </div>
            <div>
              <label>Rol</label>
              <select name="rol" id="rol" class="form-control">
                <option value="">Seleccione Rol</option>
                <?php foreach ($rol as $r) { ?>
                  <option value="<?php echo $r->id ?>"><?php echo $r->rol ?> </option>
                    <?php } ?>
              </select>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" name="GUARDAR" value="GUARDAR" class="btn btn-primary">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>

          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
  	$('#btnM').click(function(){
  		$('#modalGuardar').modal('show');
  	});
  </script>
