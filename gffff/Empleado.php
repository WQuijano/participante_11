<?php 
namespace App\Entities;
use CodeIgniter\Entity;

class Empleado extends Entity{

private $id_empleado;
private $nombre;
private $apellido;



public function getID(){
	return $this->id_empleado;
}

public function setID($id){
	$this->id_empleado = $id;
}

public function getNom(){
	return $this->nombre;
}

public function setNom($nom){
	$this->nombre = $nom;
}

public function getAp(){
	return $this->apellido;
}

public function setAp($ap){
	$this->apellido = $ap;
}



}